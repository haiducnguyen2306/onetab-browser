const electron = require('electron');

const {app, BrowserWindow, Menu} = electron;

let mainWindow;

app.on('ready', function () {
    mainWindow = new BrowserWindow({
        width: 1366,
        height: 768,
        minWidth: 960,
        minHeight: 640,
        center: true
    });
    mainWindow.loadURL(`file://${__dirname}/index.html`);

    // Menu.setApplicationMenu(null);
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
    Menu.setApplicationMenu(mainMenu);
})

const mainMenuTemplate = [
    {
        label: 'File',
        submenu:
        [
            {
                label: 'Quit',
                accelerator: 'CmdOrCtrl + Q',
                click(){
                    app.quit();
                }
            }
        ]
    },
    {
        label: 'Edit',
        submenu:
        [
            {role: 'undo'},
            {role: 'redo'},
            {type: 'separator'},
            {role: 'cut'},
            {role: 'copy'},
            {role: 'paste'},
            {role: 'delete'},
            {role: 'selectall'}
        ]
    },
    {
        label: 'View',
        submenu:
        [
            {role: 'reload'},
            {role: 'resetzoom'},
            {
                role: 'zoomin',
                accelerator: 'CmdOrCtrl+='
            },
            {role: 'zoomout',},
            {role: 'togglefullscreen'}
        ]
    },
]
