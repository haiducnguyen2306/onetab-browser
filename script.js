onload = () => {

    const webview = document.querySelector('webview');
    var backBtn = document.getElementById('backBtn');
    var forwardBtn = document.getElementById('forwardBtn');
    var web = document.getElementById('web');
    var link = document.getElementById('txtLink');

    forwardBtn.onclick = function() {
        if (webview.canGoForward()) {
            webview.goForward();
            webview.addEventListener('did-stop-loading', function() {
                document.title = webview.getTitle();
                link.value = webview.getURL();
            });
        }
    }

    backBtn.onclick = function() {
        if (webview.canGoBack()) {
            webview.goBack();
            webview.addEventListener('did-stop-loading', function() {
                document.title = webview.getTitle();
                link.value = webview.getURL();
            });
        }
    }

    link.addEventListener('keypress', function(e) {
        if (e.which == 13) {
            loadWeb();
        }
    });

    function isURL(s) {
        var regexp = /[a-zA-Z-0-9]+\.[a-zA-Z-0-9]{2,3}/;
        return regexp.test(s);
    }

    function loadWeb() {
        var l = link.value;
        if (isURL(l)) {
            if (l.startsWith("http://") || l.startsWith("https://") || l.startsWith("file://")) {
                webview.loadURL(l);
            } else {
                webview.loadURL("http://" + l);
            }
        } else {
            webview.loadURL("https://www.google.com/search?q=" + l);
        }

        webview.addEventListener('did-stop-loading', function() {
            document.title = webview.getTitle();
            link.value = webview.getURL();
        });
    }
}
